#!/usr/bin/env python3

import pstats
a = pstats.Stats('script.prof')
b = a.strip_dirs()
b.sort_stats('cumulative').print_stats(20)
b.sort_stats('tottime').print_stats(20)

